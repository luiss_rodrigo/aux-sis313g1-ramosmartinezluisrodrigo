# GRID ATTACK
## NIVEL DIFICIL


## NIVEL 1
#field {
  display: grid;
  grid-template-columns: 50% 50%;
}


## NIVEL 2
#field {
  display: grid;
  grid-template-columns: 20% 40% 40%;
}


## NIVEL 3
#field {
  display: grid;
  grid-template-columns: repeat(4, 25%);
}


## NIVEL 4
#field {
  display: grid;
  grid-template-columns: repeat(3, 30%);
}

## NIVEL 5
#field {
  display: grid;
  grid-template-columns: 100px 30%;
}


## NIVEL 6
#field {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
}


## NIVEL 7
#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
}


## NIVEL 8
#field {
  display: grid;
  grid-template-columns: 100px repeat(3, 1fr);
}


## NIVEL 9
#field {
  display: grid;
  grid-template-columns: 20% 100px 1fr;
}


## NIVEL 10
#field {
  display: grid;
  grid-template-columns: 1fr auto 1fr;
}


## NIVEL 11
#field {
  display: grid;
  grid-template-columns: 25% 50% 25%;
  grid-template-rows: 100px 150px 1fr;
}


## NIVEL 12
#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
}


## NIVEL 13
#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  column-gap: 15px
}


## NIVEL 14
#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  column-gap: 5%
}


## NIVEL 15
#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  row-gap: 40px
}


## NIVEL 16
#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  column-gap: 10px;
  row-gap: 15%;
}


## NIVEL 17
#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  gap: 20px;
}


## NIVEL 18
#field {
  display: grid;
  grid-template-columns: 1fr 100px auto;
  grid-template-rows: 1fr 1fr 100px;
  gap: 10% 20px;
}


## NIVEL 19
#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(2, 1fr);
}

#greenLand {
  grid-column-start: 3;
}


## NIVEL 20
#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(2, 1fr);
}

#greenLand {
  grid-column-start: 2;
  grid-column-end: 4;
}


## NIVEL 21
#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(2, 1fr);
  gap: 10px;
}

#greenLand {
  grid-column-start: span 2;
}


## NIVEL 22
#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: 100px 1fr;
  gap: 15px;
}

#greenLand {
  grid-column-start: span 2;
}


## NIVEL 23
#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  gap: 10px;
}

#greenLand {
  grid-row-start: 2;
}


## NIVEL 24
#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 10px;
}

#greenLand {
  grid-row-start: 1;
  grid-row-end: 5;
}


## NIVEL 25
#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 10px;
}

#greenLand {
  grid-row-start: span 4;
}


## NIVEL 26
#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  gap: 10px;
}

#redLand {
  grid-row-start: 1;
  grid-row-end: 4;
  grid-column-start: 1;
  grid-column-end: 3;
}


## NIVEL 27
#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 15px;
}

#redLand {
  grid-row-start: 3;
  grid-row-end: 5;
  grid-column-start: 2;
  grid-column-end: 4;
}


## NIVEL 28
#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 10px;
}

#greenLand {
  grid-column: 1 / 4;
  grid-row: 2 / 5;
}


## NIVEL 29
#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 15px 10px;
}

#greenLand {
  grid-column: 2 / span 2;
  grid-row: 3 / 5;
}

#redLand {
  grid-column: 4;
  grid-row: 1 / 4;
}


## NIVEL 30
#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 15px;
  grid-template-areas: "land land land ."
      "land land land ."
      "land land land ."
      ". . . .";
}

#redLand {
  grid-area: land;
}


## NIVEL 31
#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: repeat(4, 1fr);
  gap: 15px;
  grid-template-areas: "redLand redLand greenLand"
      "redLand redLand greenLand"
      "redLand redLand greenLand"
      ". . greenLand";
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}


## NIVEL 32
#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  gap: 15px 10px;
  grid-template-areas: "greenLand greenLand ."
      "greenLand greenLand ."
      "redLand redLand redLand";
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}


## NIVEL 33
#field {
  display: grid;
  grid-template-columns: min-content 1fr;
  grid-template-rows: 1fr 1fr;
}


## NIVEL 34
#field {
  display: grid;
  grid-template-columns: min-content 1fr;
  grid-template-rows: 1fr min-content;
}


## NIVEL 35
#field {
  display: grid;
  grid-template-columns: max-content 1fr;
  grid-template-rows: 1fr 1fr;
}


## NIVEL 36
#field {
  display: grid;
  grid-template-columns: max-content min-content;
  grid-template-rows: 1fr 1fr;
}


## NIVEL 37
#field {
  display: grid;
  grid-template-columns: minmax(250px, 1fr) 1fr;
  grid-template-rows: 1fr 1fr;
  gap: 15px;
}


## NIVEL 38
#field {
  display: grid;
  grid-template-columns: minmax(min-content, 200px) 150px;
  grid-template-rows: 1fr 1fr;
}


## NIVEL 39
#field {
  display: grid;
  grid-template-columns: minmax(max-content, 200px) minmax(min-content, auto);
  grid-template-rows: 1fr 1fr;
}


## NIVEL 40
#field {
  display: grid;
  grid-template-columns: repeat(auto-fill, 150px);
  gap: 15px;
}


## NIVEL 41
#field {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
  gap: 15px;
}


## NIVEL 42
#field {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
  gap: 15px;
}


## NIVEL 43
#field {
  display: grid;
  grid-template: 100px 1fr 100px / 100px 1fr 100px;
  gap: 20px;
}


## NIVEL 44
#field {
  display: grid;
  gap: 15px;
  grid-template: "redLand redLand redLand"
      ". greenLand greenLand"
      ". greenLand greenLand";
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}


## NIVEL 45
#field {
  display: grid;
  gap: 15px;
  grid-template: "redLand redLand ." 100px
      "blueLand greenLand greenLand" 200px
      "blueLand greenLand greenLand" 1fr / 1fr 1fr 1fr
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}

#blueLand {
  grid-area: blueLand;
}


## NIVEL 46
#field {
  display: grid;
  grid-template: repeat(3, 1fr) / repeat(3, 1fr);
  gap: 20px;
  grid-auto-flow: column;
}

#greenLand {
  grid-column-start: 3;
}


## NIVEL 47
#field {
  display: grid;
  grid-template: repeat(4, 1fr) / repeat(4, 1fr);
  grid-auto-flow: dense;
  gap: 20px;
}

#field div:nth-child(3n) {
  grid-column: span 3;
}


## NIVEL 48
#field {
  display: grid;
  grid-template: repeat(4, 1fr) / repeat(4, 1fr);
  grid-auto-flow: column;
  gap: 15px;
}

#greenLand {
  grid-column: span 3;
}


## NIVEL 49
#field {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-auto-columns: 1fr;
}

#redLand {
  grid-column: 3;
}


## NIVEL 50
#field {
  display: grid;
  gap: 20px;
  grid-template: 1fr 1fr / 1fr 1fr;
  grid-auto-rows: 100px;
}


## NIVEL 51
#field {
  display: grid;
  gap: 20px;
  grid-template: 1fr 1fr / 1fr 1fr;
  grid-auto-columns: 100px;
  grid-auto-rows: 100px;
}

#redLand {
  grid-column: 3;
  grid-row: 3;
}


## NIVEL 52
#field {
  display: grid;
  gap: 15px;
  grid-template: 1fr 1fr / 1fr 1fr;
  grid-auto-columns: 1fr;
  grid-auto-rows: 100px 150px;
  grid-auto-flow: column;
}

#redLand {
  grid-column: 3;
  grid-row: 4;
}


## NIVEL 53
#field {
  display: grid;
  gap: 15px;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: 100px;
  justify-items: start;
}

#field > div {
  width: 75%;
}


## NIVEL 54
#field {
  display: grid;
  gap: 15px;
  grid-template-columns: repeat(2, 1fr);
  grid-auto-rows: 100px;
  justify-items: end;
}

#field > div {
  width: 70%;
}


## NIVEL 55
#field {
  display: grid;
  grid-template: 100px 1fr / 2fr 1fr;
  justify-items: center;
}

#field > div {
  width: 50%;
}


## NIVEL 56
#field {
  display: grid;
  gap: 15px;
  grid-template: repeat(3, 1fr) / repeat(3, 1fr);
  align-items: start;
}

#field > div {
  height: 50%;
}


## NIVEL 57
#field {
  display: grid;
  grid-template: repeat(3, 1fr) / repeat(2, 1fr);
  align-items: end;
}

#field > div {
  height: 50%;
}


## NIVEL 58
#field {
  display: grid;
  grid-template: 2fr 1fr / repeat(3, 1fr);
  align-items: center;
}

#field > div {
  height: 50%;
}


## NIVEL 59
#field {
  display: grid;
  grid-template: 1fr 1fr / 2fr 1fr 1fr;
  align-items: center;
  justify-items: center;
}

#field > div {
  height: 50%;
  width: 50%;
}

#greenLand {
  grid-column: span 2;
}


## NIVEL 60
#field {
  display: grid;
  gap: 15px;
  grid-template-columns: repeat(3, 1fr);
  place-items: center end;
}

#field > div {
  height: 50%;
  width: 50%;
}


## NIVEL 61
#field {
  display: grid;
  gap: 15px;
  grid-template: 1fr 1fr / 1fr 1fr;
  justify-items: end;
}

#field > div {
  height: 50%;
  width: 50%;
}

#redLand {
  justify-self: start;
}


## NIVEL 62
#field {
  display: grid;
  gap: 15px;
  grid-template: 1fr 1fr / 1fr 1fr;
}

#redLand {
  height: 50%;
  width: 50%;
  justify-self: center;
}


## NIVEL 63
#field {
  display: grid;
  grid-template: 1fr 1fr / 200px 1fr;
  gap: 15px;
}

.redLand {
  width: 50%;
  height: 50%;
  justify-self: end;
}


## NIVEL 64
#field {
  display: grid;
  grid-template: 1fr 2fr / 1fr 2fr;
  gap: 15px;
}

.redLand {
  width: 100%;
  height: 50%;
  align-self: center;
}


## NIVEL 65
#field {
  display: grid;
  grid-template: 1fr 1fr 1fr / 1fr 1fr 1fr;
}

.redLand {
  width: 100%;
  height: 50%;
  align-self: end;
}


## NIVEL 66
#field {
  display: grid;
  gap: 15px;
  grid-template: 1fr 1fr / 2fr 1fr;
  justify-items: center;
  align-items: center;
}

#field > div {
  height: 50%;
  width: 50%;
}

#redLand {
  justify-self: end;
  align-self: end;
}

#greenLand {
  justify-self: start;
  align-self: end;
}


## NIVEL 67
#field {
  display: grid;
  grid-template: 1fr 1fr 1fr / 2fr 1fr;
  place-items: start;
}

#field > div {
  height: 50%;
  width: 50%;
}

#redLand {
  place-self: center end;
}

#greenLand {
  place-self: start end;
}


## NIVEL 68
#field {
  display: grid;
  gap: 15px;
  grid-template-columns: 150px 150px;
  justify-content: center;
}


 ## NIVEL 69
#field {
  display: grid;
  gap: 15px;
  grid-template-columns: 30% 30%;
  justify-content: end;
}


## NIVEL 70
#field {
  display: grid;
  grid-template: 1fr 1fr / 40% 40%;
  justify-content: space-evenly;
  justify-items: end;
}

#field > div {
  width: 50%;
  height: 50%;
}


## NIVEL 71
#field {
  display: grid;
  gap: 15px;
  grid-template-columns: 1fr 1fr;
  grid-auto-rows: 100px;
  align-content: center;
}


## NIVEL 72
#field {
  display: grid;
  grid-template-columns: 40% 40%;
  grid-auto-rows: 125px;
  align-content: end;
}


## NIVEL 73
#field {
  display: grid;
  grid-template-columns: 1fr 100px;
  grid-auto-rows: 100px;
  align-content: space-between;
}


## NIVEL 74
#field {
  display: grid;
  grid-template: repeat(4, 1fr) / repeat(4, 1fr);
  gap: 15px;
}

#greenLand {
  grid-row: 2 / 5;
  grid-column: 2 / 4;
}


## NIVEL 75
#field {
  display: grid;
  grid-template: repeat(3, 1fr) / repeat(3, 1fr);
  gap: 15px 10px;
  grid-template-areas: "greenLand greenLand blueLand"
      "greenLand greenLand blueLand"
      "redLand redLand redLand";
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}

#blueLand {
  grid-area: blueLand;
}


## NIVEL 76
#field {
  display: grid;
  grid-template-columns: minmax(max-content, 1fr) minmax(min-content, auto);
  grid-template-rows: 1fr 1fr;
}


## NIVEL 77
#field {
  display: grid;
  gap: 15px;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-auto-rows: 100px;
  align-content: space-between;
}

#field div:nth-child(3n) {
  grid-column: span 3;
}


## NIVEL 78
#field {
  display: grid;
  gap: 15px;
  grid-template: "greenLand . blueLand" 1fr
      "greenLand redLand ." 1fr
      / 100px 1fr 1fr;
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}

#blueLand {
  grid-area: blueLand;
  height: 50%;
  align-self: center;
}


## NIVEL 79
#field {
  display: grid;
  gap: 15px;
  grid-template: repeat(4, 1fr);
}

#greenLand {
  grid-column: 2 / span 3;
  grid-row: 3 / 5;
}

#redLand {
  grid-column: 3 / span 2;
  grid-row: 1 / 4;
}


## NIVEL 80
#field {
  display: grid;
  grid: repeat(3, 1fr);
}

#greenLand {
  grid-column: 1 / 3;
  grid-row: 1;
}

#redLand {
  grid-column: 2 / 4;
  grid-row: 1 / 3;
}

#blueLand {
  grid-column: 2;
  grid-row: 1 / 5;
}
